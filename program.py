from flask import Flask
import os

app = Flask(__name__)


@app.route("/")
def home():
    course = os.environ['COURSE']
    host = os.uname()[1]
    msg = f'Welcome to Cisco DevNet {course}!\nrunning on {host}\nUpdated!'

    return msg


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
